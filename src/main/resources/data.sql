-- password maps to 12345
INSERT INTO users (user_name, full_name , password , created_at) VALUES
  ('testuser' , 'test user' , '$2a$10$ug.lfEoJdqkb99./16Qg/OCfs694tJAvM8QL2pUs.3gYZ5jHIKVJS' , '2020-03-27 11:00:00+03');

INSERT INTO users (user_name, full_name , password , created_at) VALUES
    ('testuser2' , 'test user2' , '$2a$10$ug.lfEoJdqkb99./16Qg/OCfs694tJAvM8QL2pUs.3gYZ5jHIKVJS' , '2020-03-28 11:00:00+03');