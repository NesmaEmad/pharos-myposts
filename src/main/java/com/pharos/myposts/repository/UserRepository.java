package com.pharos.myposts.repository;


import com.pharos.myposts.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {

    //@Query(value = "SELECT * FROM users WHERE user_name = ?1 limit 1", nativeQuery = true)
    User findFirstByUserName(String user_name);

}
