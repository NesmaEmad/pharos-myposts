package com.pharos.myposts.controller;


import com.fasterxml.jackson.annotation.JsonView;
import com.pharos.myposts.View.PostView;
import com.pharos.myposts.entity.Post;
import com.pharos.myposts.model.Status;
import com.pharos.myposts.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping("/post/create")
    @JsonView(PostView.Normal.class)
    public ResponseEntity<Post> create (@RequestParam("content") String content , @RequestParam("status") Status status) {
        Post post = postService.createPost(content , status );
        return ResponseEntity.ok(post);
    }

    @GetMapping("/posts/public")
    @JsonView(PostView.Normal.class)
    public ResponseEntity<List<Post>> searchPublicPosts (@RequestParam("search_text") String searchText) {
        List<Post> posts = postService.searchPublicPosts(searchText);
        return ResponseEntity.ok(posts);
    }


}
