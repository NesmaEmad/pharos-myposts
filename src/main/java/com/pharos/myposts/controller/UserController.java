package com.pharos.myposts.controller;

import com.pharos.myposts.config.JwtTokenUtil;
import com.pharos.myposts.service.UserService;
import com.pharos.myposts.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;


import com.pharos.myposts.model.JwtRequest;
import com.pharos.myposts.model.JwtResponse;

@RestController
public class UserController {

    @Autowired
    UserService userService;


    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @RequestMapping(value = "/user/authenticate" , method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> authenticate (@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUser_name(), authenticationRequest.getPassword());

        String userName = authenticationRequest.getUser_name();
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(userName);
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));

    }


    private void authenticate(String username, String password) throws Exception {
        try {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(username, password);
            authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }



}
