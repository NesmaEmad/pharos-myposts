package com.pharos.myposts.service;


import com.pharos.myposts.entity.Post;
import com.pharos.myposts.entity.User;
import com.pharos.myposts.model.Status;

import java.util.List;

public interface PostService {
    public abstract Post createPost(String content , Status status);
    public abstract List<Post> searchPublicPosts(String searchText);
}
