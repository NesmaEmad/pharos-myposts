package com.pharos.myposts.service;


import com.pharos.myposts.entity.User;

public interface UserService {
    public abstract String getHashedPassword(String password);
    public abstract User findByUserName(String userName);
}
