package com.pharos.myposts.service;


import com.pharos.myposts.entity.Post;
import com.pharos.myposts.entity.User;
import com.pharos.myposts.model.PostSpecifications;
import com.pharos.myposts.model.Status;
import com.pharos.myposts.repository.PostRepository;
import com.pharos.myposts.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.jpa.domain.Specification.where;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Post createPost(String content, Status status) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String userName = "";
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }

        User user = userRepository.findFirstByUserName(userName);
        Post post = new Post(content , status , user);
        postRepository.save(post);
        return post;
    }


    @Override
    public List<Post> searchPublicPosts(String searchText) {
        String[] searchTextWordsArr      = searchText.split(" ");
        List<String> searchTextWordsList = Arrays.asList(searchTextWordsArr);
        List<Post> posts = postRepository
                           .findAll(where(PostSpecifications.publicPosts())
                                    .and(PostSpecifications.searchWords(searchTextWordsList)));

        return posts;
    }
}
