package com.pharos.myposts.model;

import com.pharos.myposts.entity.Post;
import org.springframework.data.jpa.domain.Specification;

import java.util.Collection;



public class PostSpecifications {

    public static Specification<Post> publicPosts() {
        return (root, query, builder) ->{
            return builder.equal(root.get("status"), Status.PUBLIC);
        };
    }

    public static Specification<Post> searchWords(Collection<String> words) {
        if(words == null || words.isEmpty()) {
            throw new RuntimeException("List of words cannot be empty.");
        }

        for (String word : words){
            if (word == null || word.equals("")){
                throw new RuntimeException("Word can't be null or empty");
            }
        }

        return (root, query, builder) -> words.stream()
                .map(String::toLowerCase)
                .map(word -> "%" + word + "%")
                .map(word -> builder.like(builder.lower(root.get("content")), word))
                .reduce(builder::or)
                .get();

    }
}
