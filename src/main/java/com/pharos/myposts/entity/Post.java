package com.pharos.myposts.entity;


import com.fasterxml.jackson.annotation.JsonView;
import com.pharos.myposts.View.PostView;
import com.pharos.myposts.model.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "posts")
public class Post implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @JsonView(PostView.Normal.class)
    private long id;
    @JsonView(PostView.Normal.class)
    private String content;

    @Enumerated(EnumType.ORDINAL)
    @JsonView(PostView.Normal.class)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonView(PostView.Normal.class)
    private User user;
    @JsonView(PostView.Normal.class)
    private Date created_at;

    public Post(String content , Status status , User user){
        this.content    = content;
        this.status     = status;
        this.user       = user;
        this.created_at = new Date();
    }

    public Post() {
    }

    public Post(long id, String content, Status status, User user, Date created_at) {
        this.id = id;
        this.content = content;
        this.status = status;
        this.user = user;
        this.created_at = created_at;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
