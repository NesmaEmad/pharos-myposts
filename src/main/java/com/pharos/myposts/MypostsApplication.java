package com.pharos.myposts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MypostsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MypostsApplication.class, args);
	}

}
