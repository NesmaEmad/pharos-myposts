TO RUN THE PROJECT , USE THESE 2 COMMANDS:
*******************************************
//this one to clear old dependencies ,
// compile the code and install any new
//packages

1) mvn clean compile install

*******************************************
//this one to run the project , the default
//port will be 8080

2) mvn spring-boot:run
